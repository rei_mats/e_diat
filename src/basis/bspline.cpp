#include "bspline.hpp"

double CalcBSpline(int degree,  const vector<double>& knots,
		   int index, double x) {

  if(degree == 0) {
    if(knots[index] <= x && x < knots[index+1])
      return 1.0;
    else
      return 0.0;
  } else {
    double ui  = knots[index];
    double ui1 = knots[index + 1];
    double uid = knots[index + degree];
    double uid1= knots[index + degree + 1];
    double bs0 = CalcBSpline(degree-1, knots, index, x);
    double bs1 = CalcBSpline(degree-1, knots, index+1, x);
    
    double acc(0.0);

    if(std::abs(bs0) > 0.000000001)
      acc += (x - ui) / (uid - ui) * bs0;
    if(abs(bs1) > 0.000000001)
      acc += (uid1 - x) / (uid1 - ui1) * bs1;
    return acc;
  }
}

double CalcDerivBSpline(int degree, const vector<double>& knots,
			int index, double x) {
  
  if(degree == 0)
    return 0.0;
  else {

    double ui  = knots[index];
    double ui1 = knots[index + 1];
    double uid = knots[index + degree];
    double uid1= knots[index + degree + 1];

    double bs0p = CalcDerivBSpline(degree-1, knots, index, x);
    double bs1p = CalcDerivBSpline(degree-1, knots, index+1, x);
    double bs0 = CalcBSpline(degree-1, knots, index, x);
    double bs1 = CalcBSpline(degree-1, knots, index+1, x);    
    
    double acc(0.0);

    if(std::abs(bs0) > 0.000000001)
      acc += 1.0 / (uid - ui) * bs0;
    if(abs(bs1) > 0.000000001)
      acc += - 1.0 / (uid1 - ui1) * bs1;
    if(abs(bs0p) > 0.00000001)
      acc += (x - ui) / (uid - ui) * bs0p;
    if(abs(bs1p) > 0.00000001)
      acc += (uid1 - x) / (uid1 - ui1) * bs1p;
    return acc;
  }
}

int NumberOfBSplineBasis(const Knots& knots, int degree) {
  // return knots.num_interval() - degree;

  //  return knots.num_point() - 2 - degree;  

  int num_interval = knots.num_seq() - 1;
  int num_span     = degree + 1;
  return num_interval - (num_span - 1);
}

void SetBSplineDiscrete(DiscreteRep*** dRep, DiscreteRep*** ddRep, 
			pKnots knots, int deg, int num_points) {
			
  vector<double> knot_list;
  knots->CreateKnotList(&knot_list);

  int num_ = NumberOfBSplineBasis(*knots, deg);
  int num_interval = knots->num_interval();
  
  *dRep = new DiscreteRep*[num_];
  *ddRep = new DiscreteRep*[num_];

  // loop for basis functions
  for(int idx_bs = 0; idx_bs < num_; idx_bs++) {

    // index of (left/right) edge of non zero region
    int idx_p0 = knots->KnotIdx2SeqIdx(idx_bs);
    int idx_p1 = knots->KnotIdx2SeqIdx(idx_bs+deg+1); 
    
    (*dRep)[idx_bs] = 
      new DiscreteRep(num_interval, 
		      idx_p0, idx_p1-1, num_points);
    (*ddRep)[idx_bs] = 
      new DiscreteRep(num_interval, 
		      idx_p0, idx_p1-1, num_points);

    // loop for non value intervals (idx_p, idx_p + 1)
    for(int idx_p = idx_p0; idx_p < idx_p1; idx_p++) {
      double x0 =knots->value_at_seq_idx(idx_p);
      double x1 =knots->value_at_seq_idx(idx_p + 1);
      for(int j = 0; j < num_points; j++) {	

	double x = GaussQuadZeroPoint(num_points,j,x0,x1);
	double y = CalcBSpline(deg,knot_list,idx_bs,x);
	double w = GaussQuadWeight(num_points, j);
	double yy= sqrt((x1-x0)/2.0 * w) * y;
	double dy= CalcDerivBSpline(deg,knot_list, idx_bs,x);
	double dyy=sqrt((x1-x0)/2.0 * w) * dy;
	(*dRep)[idx_bs]->set_value(idx_p, j, yy);
	(*ddRep)[idx_bs]->set_value(idx_p, j, dyy);
      }
    }
  }
}

BSplineSet::BSplineSet(pKnots _knots, int deg, 
		       int add_point) {
  knots_ = _knots;
  num_ = NumberOfBSplineBasis(*knots_, deg);
  deg_ = deg;
  num_point_ = deg + add_point;
  SetBSplineDiscrete(&discrete_rep_list_, 
		     &deriv_discrete_rep_list_,
		     _knots,deg, num_point_);

}
BSplineSet::~BSplineSet() {

  for(int i = 0; i < num_; i++)
    delete discrete_rep_list_[i];

  delete[] discrete_rep_list_;

}
double BSplineSet::basis_value(int i,int j,int k) const {
  return discrete_rep_list_[i]->get_value(j, k);
}
const DiscreteRep& BSplineSet::basis(int i) const {
  return *discrete_rep_list_[i];
}
void BSplineSet::Display() const {

  cout << "======== BSplineSet::Display ========" << endl;

  for(int i = 0; i < num_; i++) 
    discrete_rep_list_[i]->Display();

  cout << "=====================================" << endl;
}
void BSplineSet::CalcMatrix(const DiscreteRep& rep, 
			    Mat* mat) const {
  
  MatSetSizes(*mat, PETSC_DECIDE, PETSC_DECIDE, num_, num_);
  MatSetFromOptions(*mat);
  MatSetUp(*mat);

  for(int i = 0; i < num_; i++)
    for(int j = 0; j < num_; j++) {

      DiscreteRep* ui = discrete_rep_list_[i];
      DiscreteRep* uj = discrete_rep_list_[j];

      if(DiscreteRepExistOverlap(*ui, *uj)) {
	double ele[] = {DiscreteRepProductValue(*ui,rep,*uj)};
	MatSetValues(*mat, 1, &i, 1, &j, ele, INSERT_VALUES);
      }
    }

  /*
  int i = 1;
  double v = 1.1;
  MatSetValues(*mat, 1, &i, 1, &i, &v, INSERT_VALUES);
  */
  MatAssemblyBegin(*mat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *mat, MAT_FINAL_ASSEMBLY);
}
void BSplineSet::CalcOverlapMatrix(Mat* mat) const {

  MatSetSizes(*mat, PETSC_DECIDE, PETSC_DECIDE, num_, num_);
  MatSetFromOptions(*mat);
  MatSetUp(*mat);

  for(int i = 0; i < num_; i++)
    for(int j = 0; j < num_; j++) {

      DiscreteRep* ui = discrete_rep_list_[i];
      DiscreteRep* uj = discrete_rep_list_[j];

      if(DiscreteRepExistOverlap(*ui, *uj)) {
	double ele[] = {DiscreteRepProductValue(*ui,*uj)};
	MatSetValues(*mat, 1, &i, 1, &j, ele, INSERT_VALUES);
      }
    }

  MatAssemblyBegin(*mat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *mat, MAT_FINAL_ASSEMBLY);  
}
void BSplineSet::CalcD2Matrix(Mat* mat) const {

  MatSetSizes(*mat, PETSC_DECIDE, PETSC_DECIDE, num_, num_);
  MatSetFromOptions(*mat);
  MatSetUp(*mat);

  for(int i = 0; i < num_; i++)
    for(int j = 0; j < num_; j++) {

      DiscreteRep* ui = deriv_discrete_rep_list_[i];
      DiscreteRep* uj = deriv_discrete_rep_list_[j];

      if(DiscreteRepExistOverlap(*ui, *uj)) {
	double ele[] = { -DiscreteRepProductValue(*ui,*uj)};
	MatSetValues(*mat, 1, &i, 1, &j, ele, INSERT_VALUES);
      }
    }

  MatAssemblyBegin(*mat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(  *mat, MAT_FINAL_ASSEMBLY);    
}


