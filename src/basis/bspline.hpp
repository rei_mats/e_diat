#ifndef BSPLINE_HPP
#define BSPLINE_HPP

#include <vector>
#include <cmath>
#include <boost/shared_ptr.hpp>
#include <src/basis/discrete_rep.hpp>
#include <src/utils/quad.hpp>
#include <src/basis/knot.hpp>
#include <petsc.h>

namespace {
  using std::vector;
  using std::pair;
  using std::make_pair;
  using std::abs;
  using boost::shared_ptr;

}

class SquaredFunc {
public:

  void Calc(double x, double* y, bool* b) {
    *y = x * x; *b = true; }
  double operator() (double x) { return x * x;}
};

double CalcBSpline(int degree,  const vector<double>& knots,
		   int index, double x);
double CalcDerivBSpline(int deg, const vector<double>& knots,
			int idx, double x);

int NumberOfBSplineBasis(const Knots& knots, int degree);

/* set discrete representation for bspline*/
//void SetBSplineDiscrete(DiscreteRep**, pKnots, int);

/* calc discrete representation for function object */
template<class F>
pDiscreteRep NewDiscreteFunction(F f, const Knots& knots, 
				 int num_point) {

  int num_interval = knots.num_interval();
  DiscreteRep* ptr = 
    new DiscreteRep(num_interval,0, num_interval, num_point);

  for(int i = 0; i < num_interval; i++) {
    double x0 = knots.value_at_seq_idx(i);
    double x1 = knots.value_at_seq_idx(i+1);

    for(int j = 0; j < num_point; j++) {
      double x = GaussQuadZeroPoint(num_point, j, x0, x1);
      ptr->set_value(i, j, f(x));
    }
  }
  
  return pDiscreteRep(ptr);
}

class BSplineSet {
  /* ========== Constructors =======================*/
public:

  // i means additional quadrature point. 
  BSplineSet(pKnots, int, int i = 1);
  ~BSplineSet();
  
  /* =========== Pure Functional ====================*/
  double basis_value(int, int, int) const;
  const DiscreteRep& basis(int) const;
  inline int num() const { return num_; }
  void Display() const;
  template<class F>
  pDiscreteRep DiscretizeFunction(F f) const {
    return NewDiscreteFunction(f, *knots_, num_point_);
  }
  void CalcMatrix(const DiscreteRep& rep, Mat* mat) const;
  void CalcOverlapMatrix(Mat* mat) const;
  void CalcD2Matrix(Mat* mat) const;

private:
  /* =========== Member Field ======================*/
  pKnots knots_;
  int num_;
  int deg_;
  int num_point_;
  DiscreteRep** discrete_rep_list_;
  DiscreteRep** deriv_discrete_rep_list_;
};

#endif
