#include "discrete_rep.hpp"

Intervals::Intervals(const vector<double>& xs) {
    
    for(vector<double>::const_iterator it1 = xs.begin(), 
	  it2 = xs.begin(), it_end = xs.end();
	(it1+1) != it_end; ++it1) {
      it2 = it1 + 1;
      dd_list_.push_back(make_pair(*it1, *it2));
    }
  }

DiscreteRep::DiscreteRep(int _num_i, int i0, int i1,  
			 int _num_point) {
  num_interval_ = _num_i;
  num_point_    = _num_point;
  data_ = new double*[_num_i];

  for(int i = 0; i < i0; i++)
    data_[i] = NULL;
  for(int i = i0; i < i1 + 1; i++)
    data_[i] = new double[num_point_];
  for(int i = i1+1; i < num_interval_; i++)
    data_[i] = NULL;  
}

DiscreteRep::~DiscreteRep() {
  for(int i = 0; i < num_interval_; i++)
      if(data_[i])
	  delete[] data_[i];

  delete[] data_;
}

void DiscreteRep::Display() const {

 cout << "======== DiscreteRep::Display ========" << endl;
 for(int i = 0; i < num_interval_; i++) {

   cout << "interval_" << i << " :  ";
   if(data_[i])  
     for(int p = 0; p < num_point_; p++)
       cout << data_[i][p] << " ";
   cout << endl;
 }
 cout << "=====================================" << endl;
}

void DiscreteRepProduct(const DiscreteRep& a, 
			const DiscreteRep& b, 
			double* res, bool* has_value) {

  *res = 0.0;
  *has_value = false;

  if( a.get_num_point() != b.get_num_point() || 
      a.get_num_interval() != b.get_num_interval())
    return;

  for(int i = 0; i < a.get_num_interval(); i++) {
    if(a.data_[i] && b.data_[i]) {
      *has_value = true;
      for(int j = 0; j < a.get_num_point();j++)
	*res += a.data_[i][j] * b.data_[i][j];
    }
  }
}

void DiscreteRepProduct(const DiscreteRep& a, 
			const DiscreteRep& b, 
			const DiscreteRep& c, 
			double* res, bool* has_value) {
  *res = 0.0;
  *has_value = false;

  if( a.get_num_point() != b.get_num_point() || 
      a.get_num_point() != c.get_num_point() || 
      a.get_num_interval() != b.get_num_interval() ||
      a.get_num_interval() != c.get_num_interval() )
    return;

  for(int i = 0; i < a.get_num_interval(); i++) {
    if(a.data_[i] && b.data_[i] && c.data_[i]) {
      *has_value = true;
      for(int j = 0; j < a.get_num_point();j++)
	*res += a.data_[i][j] * b.data_[i][j] *c.data_[i][j];
    }
  }
}

double DiscreteRepProductValue(const DiscreteRep& a, 
			       const DiscreteRep& b) {

  double acc = 0.0;
  for(int i = 0; i < a.get_num_interval(); i++) {
    if(a.data_[i] && b.data_[i]) {
      for(int j = 0; j < a.get_num_point();j++)
	acc += a.data_[i][j] * b.data_[i][j];
    }
  }
  return acc;
}

double DiscreteRepProductValue(const DiscreteRep& a, 
			       const DiscreteRep& b, 
			       const DiscreteRep& c) {

  double acc = 0.0;
  for(int i = 0; i < a.get_num_interval(); i++) {
    if(a.data_[i] && b.data_[i] && c.data_[i]) {
      for(int j = 0; j < a.get_num_point();j++)
	acc += a.data_[i][j] * b.data_[i][j] * c.data_[i][j];
    }
  }
  return acc;
}


void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&);
void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&, const DiscreteRep&);

bool DiscreteRepExistOverlap(const DiscreteRep& a, 
			     const DiscreteRep& b) {
  if( a.get_num_point() != b.get_num_point() || 
      a.get_num_interval() != b.get_num_interval())
    return false;

  for(int i = 0; i < a.get_num_interval(); i++)
    if(a.data_[i] && b.data_[i])
      return true;

  return false;
}




