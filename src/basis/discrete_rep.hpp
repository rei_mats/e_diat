#ifndef DISCRETE_REP_HPP
#define DISCRETE_REP_HPP

#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
//#include <boost/optional.hpp>

/**
 * Discrete representation for functions using 
 * some intervals;
 * {null, {f_10, f_11,...}, {f_20, f_21,...}, null,..}
 * null is represented by NULL pointer.
 */

namespace
{
  using std::cout;
  using std::endl;
  using std::vector;
  using std::pair;
  using std::make_pair;
  using boost::shared_ptr;
  //  using boost::optional;
  typedef vector<pair<double, double> > DDs;
}

class Intervals;
typedef shared_ptr<Intervals> pIntervals;

class Intervals {
private:
  DDs dd_list_;

public:
  /* ============ minor type ============================= */
  typedef DDs::const_iterator const_iterator;

  /* ============ Constructor =============================*/
  Intervals(const vector<double>& xs);
  static pIntervals New(const vector<double>& xs) {
    Intervals* ptr = new Intervals(xs);
    return pIntervals(ptr);
  }

  /* ============ Getter ==================================*/
  int size() const { return dd_list_.size(); }
  inline double left_edge(int i) { 
    return dd_list_[i].first; }
  inline double right_edge(int i) { 
    return dd_list_[i].second; }
  DDs::const_iterator begin() const { 
    return dd_list_.begin(); }
  DDs::const_iterator end() const { 
    return dd_list_.end(); }
  DDs::const_iterator it_at(int n) const {
    return dd_list_.begin() + n; }
};

class DiscreteRep
{
private:
  /* # of intervals */
  int      num_interval_;
  /* # of point for each intervals*/
  int      num_point_;     
  /* data */
  double** data_;

public:
  /* ========== Constructors ======================= */
  DiscreteRep(int _num_interval, int i0, int i1, int _num_point);
  ~DiscreteRep();

  /* ========== Accessor =========================== */
  inline void set_value(int i, int j, double v) { 
    data_[i][j] = v;
  }
  inline double get_value(int i, int j) const {
    if(data_[i])
      return data_[i][j];
    else
      return 0.0; }
  inline int get_num_interval() const {return num_interval_;}
  inline int get_num_point() const { return num_point_;}
  void Display() const;

  friend void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&, double*, bool*);
  friend void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&, const DiscreteRep&, double*, bool*);

  friend double DiscreteRepProductValue(const DiscreteRep&, const DiscreteRep&);
  friend double DiscreteRepProductValue(const DiscreteRep&, const DiscreteRep&, const DiscreteRep&);
  friend bool DiscreteRepExistOverlap(const DiscreteRep&, const DiscreteRep&);
};

typedef shared_ptr<DiscreteRep> pDiscreteRep;

void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&, double*, bool*);
void DiscreteRepProduct(const DiscreteRep&, const DiscreteRep&, const DiscreteRep&, double*, bool*);

double DiscreteRepProductValue(const DiscreteRep&, const DiscreteRep&);
double DiscreteRepProductValue(const DiscreteRep&, const DiscreteRep&, const DiscreteRep&);

bool DiscreteRepExistOverlap(const DiscreteRep&, const DiscreteRep&);

/*
template<class F>
pDiscreteRep NewDiscreteRep(const Intervals& intervals,
			    const vector<double>& xs_in_m1p1,
			    int i0, int i1, F f) {
  
  int num_interval = intervals.size();
  int num_point    = xs_in_m1p1.size();
  DiscreteRep* ptr = 
    new DiscreteRep(num_interval, i0, i1, num_point);

  for(int i = i0; i < i1; i++) {

    double x0 = intervals->left_edge(i);
    double x1 = intervals->right_edge(i);

    for(int j = 0; j < num_point; j++) {
      double x = xs_in_m1p1[j] * (x1-x0)/2.0 + (x1+x0)/2.0;
      double y = f(x);
      ptr->set_value(i, j, y);
    }
  }

  return pDiscreteRep(ptr);
}
*/

#endif
