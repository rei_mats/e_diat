#include "knot.hpp"

Knots::Knots(int num) {
  mult_value_list_.resize(num);
}
pKnots Knots::NewUniform(int num0, int num, 
			 double x0, double dx) {
  Knots* ptr = new Knots(num);

  ptr->Add(0, num0, x0);
  for(int i = 1; i < num-1; i++)
    ptr->Add(i, 1, x0 + i * dx);
  ptr->Add(num-1, num0, x0 + (num-1) * dx);

  return pKnots(ptr);
}
void Knots::Add(int i, int num, double val) {
  mult_value_list_[i] = make_pair(num,val);
}
void Knots::CreateKnotList(vector<double>* res) const {
  for(IDIt it = mult_value_list_.begin(),
	it_end = mult_value_list_.end();
      it != it_end; ++it) {
    for(int i = 0; i < it->first; i++)
      res->push_back(it->second);
  }
}
int Knots::KnotIdx2SeqIdx(int k_idx) const {

  int acc_knot_idx  = 0;
  int acc_point_idx = 0;

  for(IDIt it    = mult_value_list_.begin(),
	it_end = mult_value_list_.end();
      it != it_end; ++it) {

    acc_knot_idx += it->first;

    if(acc_knot_idx > k_idx)
      return acc_point_idx;

    acc_point_idx++;

  }
  return -1;
}
void Knots::Display() const {

  cout << "======== Knots::Display ========" << endl;
  for(IDIt it = mult_value_list_.begin(),
	it_end = mult_value_list_.end();
      it != it_end; ++it) {

      int multi = it->first;
      double xi = it->second;
      for(int i = 0; i < multi; i++)
	cout << xi << " ";
  }
  cout << endl;
  cout << "=================================" << endl;
}
