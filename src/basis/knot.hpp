#ifndef KNOT_HPP
#define KNOT_HPP

#include <vector>
#include <iostream>
#include <boost/shared_ptr.hpp>

namespace {
  using std::vector;
  using std::pair;
  using std::make_pair;
  using std::cout;
  using std::endl;
  typedef vector<pair<int, double> >::const_iterator IDIt;
  using boost::shared_ptr;
}

class Knots;
typedef shared_ptr<Knots> pKnots;

/**
 * data structure such that
 * v0 = u0 = u1 = u2 = u3 < 
 * v1 = u4 = u5 <
 * v2 = u6 <
 * v3 = u7 = u8 = u9
 * {vi} is called sequence
 * {ui} is callled knot
 */
class Knots {
public:
  /* ========== Constructor =========================*/
  Knots(int);
  static pKnots NewUniform(int,int,double,double);

  /* ========== Pure Functional =====================*/
  inline int multiplicity(int i) const { 
    return mult_value_list_[i].first; }
  inline double value_at_seq_idx(int i) const {
    return mult_value_list_[i].second; }
  inline int num_seq() const { 
    int acc = 0;
    for (IDIt it = mult_value_list_.begin(),
	   it_end = mult_value_list_.end();
	 it != it_end; ++it)
      acc += it->first;
    return acc; 
  }
  inline int num_interval() const { 
    return mult_value_list_.size() - 1; }
  int KnotIdx2SeqIdx(int k_idx) const;
  void Display() const;
  void CreateKnotList(vector<double>*) const;
  
  /* ========== Side Effect =========================*/
  void Add(int i, int num, double val);

private:
 /* ========== Member Field =========================*/
 vector<pair<int, double> > mult_value_list_;
  
};

#endif
