#include <src/utils/timer.hpp>
#include <src/utils/key_val.hpp>
#include <src/basis/bspline.hpp>
#include <src/basis/discrete_rep.hpp>

#include <gtest/gtest.h>
#include <petsc.h>
#include <petscsys.h>
#include <petscvec.h>

class HLike {
public:
  HLike(double _z, int _l) :
    z(_z), l(_l) {}
  double z;
  int l;
  double operator() (double r) { 
    return l * (l + 1) / (2.0 * r * r) - z /r;
  }
};

int main (int argc, char** args) {

  if(argc == 1) 
    return 1;
  
  PetscInitialize(&argc, &args, (char*)0, "compte hydrogen like atom problem using B-Splines");

  /* ------- read file ------ */
  KeyVal key_val;
  std::ifstream ifs(args[1]);
  key_val.Read(ifs);
  double z       = key_val.Get<double>("z");
  int    l       = key_val.Get<int>(   "l");
  double bs_xmax = key_val.Get<double>("bs_xmax");
  int    bs_num  = key_val.Get<int>(   "bs_num");
  int    bs_n0   = key_val.Get<int>(   "bs_n0");
  int    bs_deg  = key_val.Get<int>(   "bs_deg");

  /* -------- object ----------- */
  double dx = bs_xmax / bs_num;
  pKnots knots = Knots::NewUniform(bs_n0, bs_num, 0.0, dx);
  BSplineSet bs_set(knots, bs_deg);
  HLike h_like(z, l);
  pDiscreteRep v_rep = bs_set.DiscretizeFunction(h_like);

  /* -------- matrix ------------ */
  Mat smat;
  Mat vmat;
  Mat hmat;

  MatCreate(PETSC_COMM_WORLD, &smat);  
  MatCreate(PETSC_COMM_WORLD, &vmat);
  MatCreate(PETSC_COMM_WORLD, &hmat);

  bs_set.CalcOverlapMatrix(&smat);
  bs_set.CalcMatrix(*v_rep, &vmat);
  bs_set.CalcD2Matrix(&hmat);

  MatScale(hmat, -0.5);
  MatAXPY(hmat, 1.0, vmat, DIFFERENT_NONZERO_PATTERN);

  /* --------- write ------------- */
  PetscViewer viewer_hmat, viewer_smat;
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, "hmat.dat", 
			FILE_MODE_WRITE, &viewer_hmat);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD, "smat.dat", 
			FILE_MODE_WRITE, &viewer_smat);
    MatView(hmat, viewer_hmat);
    MatView(smat, viewer_smat);

  /* -------- finalize ------------*/
  MatDestroy(&smat);
  MatDestroy(&vmat);
  MatDestroy(&hmat);
  cout << "a" << endl;
  PetscFinalize();
  cout << "b" << endl;
}
