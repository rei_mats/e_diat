#include <petsc.h>
#include <petscsys.h>
#include <petscvec.h>

#include "slepcesps.h"

int main (int argc, char** args) {
  SlepcInitialize(&argc, &args, (char*)0, "Read two binary matrix and solve eigenvalue problem.");
  PetscPrintf(PETSC_COMM_WORLD, "Hello!\n");
  SlepcFinalize();
}

