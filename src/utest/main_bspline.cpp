#include <src/basis/bspline.hpp>
#include <src/basis/discrete_rep.hpp>
#include <src/utils/timer.hpp>
#include <gtest/gtest.h>
#include <petsc.h>
#include <petscsys.h>
#include <petscvec.h>

int main(int argc, char** args) {

  PetscInitialize(&argc, &args, (char*)0, "aac");

  // init bsplines
  pKnots knots = Knots::NewUniform(2, 5, 0.0, 1.1);
  BSplineSet bs(knots, 2,  2);

  // discretized function
  SquaredFunc f;
  pDiscreteRep f_rep = bs.DiscretizeFunction(f);

  // matrix 
  Mat smat;
  Mat d2mat;
  Mat mat;

  MatCreate(PETSC_COMM_WORLD, &smat);  
  MatCreate(PETSC_COMM_WORLD, &d2mat);
  MatCreate(PETSC_COMM_WORLD, &mat);
  
  // calc!
  bs.CalcOverlapMatrix(&smat);
  bs.CalcD2Matrix(&d2mat);
  bs.CalcMatrix(*f_rep, &mat);
  
  // view
  MatView(smat,  PETSC_VIEWER_STDOUT_WORLD);
  MatView(d2mat, PETSC_VIEWER_STDOUT_WORLD);
  MatView(mat,   PETSC_VIEWER_STDOUT_WORLD);

  MatDestroy(&mat);
  MatDestroy(&d2mat);
  MatDestroy(&smat);
  PetscFinalize();

}


