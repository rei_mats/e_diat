
TEST(TEST_optional, easy)
{
  boost::optional<int> a = 5;
  boost::optional<int> b;

  bool has_val_a = false;
  bool has_val_b = false;
  if(a)
    has_val_a = true;
  if(b)
    has_val_b = true;

  EXPECT_TRUE( has_val_a);
  EXPECT_FALSE(has_val_b);
} 

TEST(TEST_Time, vector_vs_array)
{
  int num = 100000;
  Timer timer;
  timer.Start("array");
  double* xs = new double[num];
  for(int i = 0; i < num; i++)
    xs[i] = i * 1.0;
  timer.End("array");
   
  timer.Start("vector1");
  vector<double> vec;
  vec.resize(num);
  for(int i = 0; i < num; i++)
    vec[i] = i * 1.0 * 2;
  timer.End("vector1");

  timer.Start("vector2");
  vector<double> vec2;
  vec2.resize(num);
  for(vector<double>::iterator it = vec2.begin(),
	it_end = vec2.end();
      it != it_end; ++it)
    *it = 1.0;
  timer.End("vector2");

  timer.Display();
  delete[] xs;
}

TEST(TEST_ArrayData, time_compare)
{
  int num = 1000;
  Timer timer;
  //  PetscInitialize(0, 0, (char*)0, "aiu");

  timer.Start("array");
  vector<double*> xs2;
  xs2.resize(num);
  for(int i = 0; i < num; i++)
    {
      xs2[i] = new double[num];
      for(int j = 0; j < num; j++)
	xs2[i][j] = 1.0;
    }
  timer.End("array");

  timer.Start("vector");
  vector<vector<double> > xs3;
  xs3.resize(num);
  for(vector<vector<double> >::iterator it = xs3.begin(),
	it_end = xs3.end(); it != it_end; ++it)
    {
      it->resize(num);
      for(vector<double>::iterator it2 = it->begin(),
	    it2_end = it->end(); it2 != it2_end; ++it2)
	*it2 = 1.0;
    }
  timer.End("vector");

  timer.Display();
}

