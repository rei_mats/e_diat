#include <src/basis/bspline.hpp>
#include <src/basis/discrete_rep.hpp>
#include <src/basis/knot.hpp>
#include <src/utils/timer.hpp>
#include <gtest/gtest.h>
#include <petsc.h>
#include <petscsys.h>
#include <petscvec.h>

TEST(CalcBSpline, zero_th) {
  vector<double> knots;
  for(int i = 0; i < 10; i++)
    knots.push_back( i * 1.0);

  EXPECT_DOUBLE_EQ(1.0,
		   CalcBSpline(0, knots, 2, 2.5));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcBSpline(0, knots, 2, 3.5));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcBSpline(0, knots, 2, 1.5));
}

TEST(CalcBSpline, first_th) {
  vector<double> knots;
  for(int i = 0; i < 10; i++)
    knots.push_back( i * 1.0);

  EXPECT_DOUBLE_EQ(0.0,
		   CalcBSpline(1, knots, 2, 1.5));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcBSpline(1, knots, 2, 2.0));
  EXPECT_DOUBLE_EQ(0.5,
		   CalcBSpline(1, knots, 2, 2.5));
  EXPECT_DOUBLE_EQ(1.0,
		   CalcBSpline(1, knots, 2, 3.0));
  EXPECT_DOUBLE_EQ(0.5,
		   CalcBSpline(1, knots, 2, 3.5));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcBSpline(1, knots, 2, 4.0));
}

TEST(CalcBSpline, third) {
  vector<double> knots;
  for(int i = 0; i < 10; i++)
    knots.push_back( i * 1.0);

 EXPECT_NEAR(0.509729,
	     CalcBSpline(3, knots, 2, 3.55),
	     0.000001);
}

TEST(CalcBSpline, Multicity) {
  // {0,0 1.1, 2.2, 3.3, 4.4,4.4}
  vector<double> us;
  us.resize(7);
  us[0] = 0.0; us[1] = 0.0; us[2] = 1.1;
  us[3] = 2.2; us[4] = 3.3; us[5] = 4.4; us[6] = 4.4;

  EXPECT_NEAR(1.0,
	      CalcBSpline(0, us, 2, 1.5),
	      0.0000001);

  EXPECT_NEAR(0.727272727,
	      CalcBSpline(1, us, 0, 0.3),
	      0.000001);
  EXPECT_NEAR(0.2727272727,
	      CalcBSpline(1, us, 1, 0.3),
	      0.000001);

  EXPECT_NEAR(0.363636,
	      CalcBSpline(1, us, 2, 1.5),
	      0.000001);

  EXPECT_NEAR(0.644628,
	      CalcBSpline(2, us, 0, 0.6),
	      0.000001);
  EXPECT_NEAR(0.14876,
	      CalcBSpline(2, us, 1, 0.6),
	      0.000001);
  EXPECT_NEAR(0.0,
	      CalcBSpline(2, us, 2, 0.6),
	      0.000001);

  EXPECT_NEAR(0.0661157,
	      CalcBSpline(2, us, 2, 1.5),
	      0.000001);

  EXPECT_NEAR(0.390558,
	      CalcBSpline(3, us, 1, 1.5),
	      0.000001);

  EXPECT_NEAR(0.537002,
	      CalcBSpline(3, us, 0, 1.5),
	      0.000001);

  EXPECT_NEAR(0.00801402,
	      CalcBSpline(3, us, 2, 1.5),
	      0.000001);
}

TEST(CalcBSpline, NumberOfBSplineBasis) {
  pKnots knots = Knots::NewUniform(2, 5, 0.0, 1.1);

  EXPECT_EQ(6, NumberOfBSplineBasis(*knots, 0));
  EXPECT_EQ(5, NumberOfBSplineBasis(*knots, 1));
  EXPECT_EQ(4, NumberOfBSplineBasis(*knots, 2));
  EXPECT_EQ(3, NumberOfBSplineBasis(*knots, 3));
  EXPECT_EQ(2, NumberOfBSplineBasis(*knots, 4));
  EXPECT_EQ(1, NumberOfBSplineBasis(*knots, 5));
}

TEST(CalcDerivBSpline, simple) {
  vector<double> knots;
  for(int i = 0; i < 10; i++)
    knots.push_back( i * 1.0);
  EXPECT_DOUBLE_EQ(0.0,
		   CalcDerivBSpline(0, knots, 1,0.1));

  EXPECT_DOUBLE_EQ(+1.0,
		   CalcDerivBSpline(1, knots, 0, 0.1));
  EXPECT_DOUBLE_EQ(-1.0,
		   CalcDerivBSpline(1, knots, 0, 1.1));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcDerivBSpline(1, knots, 0, 2.1));

  EXPECT_DOUBLE_EQ(0.0,
		   CalcDerivBSpline(1, knots, 1, 0.1));
  EXPECT_DOUBLE_EQ(1.0,
		   CalcDerivBSpline(1, knots, 1, 1.1));
  EXPECT_DOUBLE_EQ(-1.0,
		   CalcDerivBSpline(1, knots, 1, 2.1));
  EXPECT_DOUBLE_EQ(0.0,
		   CalcDerivBSpline(1, knots, 1, 3.1));

  EXPECT_DOUBLE_EQ(0.111,
		   CalcDerivBSpline(2, knots, 0, 0.111));  
  EXPECT_DOUBLE_EQ(1.0-0.111 * 2 ,
		   CalcDerivBSpline(2, knots, 0, 1.111));  
  EXPECT_DOUBLE_EQ(-1.0+0.111,
		   CalcDerivBSpline(2, knots, 0, 2.111));  
  EXPECT_DOUBLE_EQ(0.0,
		   CalcDerivBSpline(2, knots, 0, 3.111));  
}

TEST(CalcDerivBSpline, multiplicity) {

  // {0,0 1.1, 2.2, 3.3, 4.4,4.4}
  vector<double> us;
  us.resize(7);
  us[0] = 0.0; us[1] = 0.0; us[2] = 1.1;
  us[3] = 2.2; us[4] = 3.3; us[5] = 4.4; us[6] = 4.4;

  EXPECT_NEAR(0.227273,
	      CalcDerivBSpline(3, us, 0, 0.1),
	      0.000001);
  EXPECT_NEAR(0.227273,
	      CalcDerivBSpline(3, us, 0, 1.1),
	      0.000001);
  EXPECT_NEAR(-0.524042,
	      CalcDerivBSpline(3, us, 0, 2.1),
	      0.000001);
}

TEST(Knots, Construct) {
  // 0, 0, 0, 1, 2, 3, 4, 4, 4
  pKnots knots = Knots::NewUniform(3, 5, 0.0, 1.1);
  EXPECT_EQ(3, knots->multiplicity(0));
  EXPECT_EQ(1, knots->multiplicity(1));
  EXPECT_EQ(3, knots->multiplicity(4));
  EXPECT_DOUBLE_EQ(0.0, knots->value_at_seq_idx(0));
  EXPECT_DOUBLE_EQ(1.1, knots->value_at_seq_idx(1));
}

TEST(Knots, CreateKnotList) {
  pKnots knots = Knots::NewUniform(3, 5, 0.0, 1.1);
  vector<double> knots_list;
  knots->CreateKnotList(&knots_list);
  EXPECT_EQ(0.0, knots_list[0]);
  EXPECT_EQ(0.0, knots_list[1]);
  EXPECT_EQ(1.1, knots_list[3]);
}

TEST(Knots, point_knot) {
  // (0,0,0, 1, 2, 3, 4,4,4)
  pKnots knots = Knots::NewUniform(3, 5, 0.0, 1.0);
  EXPECT_EQ(4, knots->num_interval());

  EXPECT_EQ(0, knots->KnotIdx2SeqIdx(0));
  EXPECT_EQ(0, knots->KnotIdx2SeqIdx(1));
  EXPECT_EQ(0, knots->KnotIdx2SeqIdx(2));
  EXPECT_EQ(1, knots->KnotIdx2SeqIdx(3));
  EXPECT_EQ(2, knots->KnotIdx2SeqIdx(4));
  EXPECT_EQ(3, knots->KnotIdx2SeqIdx(5));
  EXPECT_EQ(4, knots->KnotIdx2SeqIdx(6));
  EXPECT_EQ(4, knots->KnotIdx2SeqIdx(7));
  EXPECT_EQ(4, knots->KnotIdx2SeqIdx(8));

  knots = Knots::NewUniform(2, 5, 0.0, 1.0);
  EXPECT_EQ(4, knots->num_interval());

  EXPECT_EQ(0, knots->KnotIdx2SeqIdx(0));
  EXPECT_EQ(1, knots->KnotIdx2SeqIdx(2));

  EXPECT_EQ(0, knots->KnotIdx2SeqIdx(1));
  EXPECT_EQ(2, knots->KnotIdx2SeqIdx(3));
}

/*
TEST(Knots, Practice_CalcDiscreteFunction) {


  
  pKnots knots = Knots::NewUniform(3, 5, 0.0, 1.0);
  pIntervals intervals = knots->Interval();
  int num_point(3);
  vector<double> x0s = GaussQuadZeroPoint(2);
  SquaredFunc f;
  

  for(Interval::iterator it = intervals.begin();
      it != itervals.end(); ++it) {

    double x0 = it->first;
    double x1 = it->second;

    for(vector<double>::iterator x_it = x0s.begin();
	x_it != x0s.end(); ++x_it) {
      double x = (x1-x0)/2.0 *(*x_it) + (x1+x0)/2.0;
      double y; bool b;
      f.Calc(x, &y, &b);
    }

  }
  
  DiscreteRep* f_disc = NewDiscreteFunction(f, *knots, 3);
  f_disc->Display();  
  delete f_disc;
}
*/

TEST(BSplineSet, Construct) {
  pKnots knots = Knots::NewUniform(3, 5, 0.0, 1.1);
  int deg(0);
  BSplineSet* bs = NULL;
  EXPECT_NO_THROW(bs = new BSplineSet(knots, deg));
  EXPECT_NO_THROW(delete bs);
}

TEST(BSplineSet, simple_zero) {
  // {0, 1, 2, 3, 4}
  pKnots knots = Knots::NewUniform(1, 5, 0.0, 1.0);
  BSplineSet bs(knots, 0);

  EXPECT_EQ(4, bs.num());

  // 0 th basis, 0 th interval, 0 th point.
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++) {
      double d; bool b;
      DiscreteRepProduct(bs.basis(i), bs.basis(j), &d, &b);  
      double x = bs.basis_value(i, j, 0);
      if(i == j) {
	EXPECT_DOUBLE_EQ(1.0, x);
	EXPECT_DOUBLE_EQ(1.0, d);
	EXPECT_TRUE(b);
      } else {
	EXPECT_DOUBLE_EQ(0.0, x);
	EXPECT_DOUBLE_EQ(0.0, d);
	EXPECT_FALSE(b);
      }
    }
}

TEST(BSplineSet, simple_one) {
  // {0, 1, 2, 3, 4}
  pKnots knots = Knots::NewUniform(1, 5, 0.0, 1.0);
  BSplineSet bs(knots, 1);

  double eps(0.000001);
  double d; bool b;

  DiscreteRepProduct(bs.basis(0), bs.basis(0), &d, &b);
  EXPECT_NEAR(2.0/3.0, d, eps);

  DiscreteRepProduct(bs.basis(1), bs.basis(0), &d, &b);
  EXPECT_NEAR(1.0/6.0, d, eps);

  DiscreteRepProduct(bs.basis(1), bs.basis(1), &d, &b);
  EXPECT_NEAR(2.0/3.0, d, eps);
  
  DiscreteRepProduct(bs.basis(2), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.0, d, eps);
}

TEST(BSplineSet, simple_one_int) {

  pKnots knots = Knots::NewUniform(1, 5, 0.0, 1.0);
  BSplineSet bs(knots, 1, 2);

  SquaredFunc f;
  pDiscreteRep f_rep = bs.DiscretizeFunction(f);
  
  double d; bool b;
  DiscreteRepProduct(*f_rep, bs.basis(0), bs.basis(0),&d, &b);
  EXPECT_NEAR(11.0/15.0, d, 0.0000001);

  DiscreteRepProduct(*f_rep, bs.basis(1), bs.basis(0),&d, &b);
  EXPECT_NEAR(23.0/60.0, d, 0.0000001);

  DiscreteRepProduct(*f_rep, bs.basis(2), bs.basis(0),&d, &b);
  EXPECT_NEAR(0.0, d, 0.0000001);

}

TEST(BSplineSet, simple_two) {
  // {0, 1, 2, 3, 4}
  pKnots knots = Knots::NewUniform(1, 5, 0.0, 1.0);
  BSplineSet bs(knots, 2);

  double eps(0.000001);
  double d; bool b;

  DiscreteRepProduct(bs.basis(0), bs.basis(0), &d, &b);
  EXPECT_NEAR(11.0/20.0, d, eps);

  DiscreteRepProduct(bs.basis(1), bs.basis(0), &d, &b);
  EXPECT_NEAR(13.0/60.0, d, eps);

  DiscreteRepProduct(bs.basis(1), bs.basis(1), &d, &b);
  EXPECT_NEAR(11.0/20.0, d, eps);
}

TEST(BSplineSet, multi_one) {
  pKnots knots = Knots::NewUniform(2, 5, 0.0, 1.1);
  BSplineSet bs(knots, 1);
  double eps(0.000001);
  double d; bool b;

  DiscreteRepProduct(bs.basis(0), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.366666666666, d, eps);
  DiscreteRepProduct(bs.basis(1), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.1833333333333, d, eps);
  DiscreteRepProduct(bs.basis(1), bs.basis(1), &d, &b);
  EXPECT_NEAR(0.7333333333333, d, eps);
  DiscreteRepProduct(bs.basis(0), bs.basis(2), &d, &b);
  EXPECT_FALSE(b);
  
}

TEST(BSplineSet, multi_three) {
  // {0,0 1.1, 2.2, 3.3, 4.4,4.4}
  pKnots knots = Knots::NewUniform(2, 5, 0.0, 1.1);
  EXPECT_EQ(7, knots->num_seq());

  BSplineSet bs(knots, 3);

  EXPECT_EQ(3, bs.num());

  double eps(0.000001);
  double d; bool b;
  DiscreteRepProduct(bs.basis(0), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.359464, d, eps);
  
  DiscreteRepProduct(bs.basis(1), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.247063, d, eps);  

  DiscreteRepProduct(bs.basis(1), bs.basis(1), &d, &b);
  EXPECT_NEAR(0.527302, d, eps);  

  DiscreteRepProduct(bs.basis(2), bs.basis(0), &d, &b);
  EXPECT_NEAR(0.0259722, d, eps);  

  DiscreteRepProduct(bs.basis(2), bs.basis(1), &d, &b);
  EXPECT_NEAR(0.247063, d, eps);  

  DiscreteRepProduct(bs.basis(2), bs.basis(2), &d, &b);
  EXPECT_NEAR(0.359464, d, eps);
}

/*
TEST(BSplineSet, pretest_mat) {
  int argc = 1;
  char** args;
  
  PetscInitialize(&argc, &args, (char*)0, "aac");

  Mat mat;
  MatCreate(PETSC_COMM_WORLD, &mat);
  
  MatSetSizes(mat, PETSC_DECIDE, PETSC_DECIDE, 5, 5);
  MatSetFromOptions(mat);
  MatSetUp(mat);

  double vals[3];
  vals[0] = 1.1; vals[1] = 2.0; vals[2] = 2.0;
  int i = 1;

  MatSetValues(mat, 1, &i, 1, &i, vals, INSERT_VALUES);
    //PetscScalar v = 1.1;
  //  MatSetValue(mat, 0, 0, v , INSERT_VALUES);
  MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);

  MatView(mat, PETSC_VIEWER_STDOUT_WORLD);

  MatDestroy(&mat);
  PetscFinalize();
}
*/



