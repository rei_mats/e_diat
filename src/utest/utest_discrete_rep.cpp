#include <src/basis/bspline.hpp>
#include <src/basis/discrete_rep.hpp>
#include <src/utils/timer.hpp>
#include <gtest/gtest.h>

class SquaredFunc1 {
public:
  void Calc(double x, double* y, bool* b) {
    *y = x * x;
    *b = true;
  }
};

TEST(TEST_Intervals, construct) {
  vector<double> xs;
  xs.push_back(0.0);
  xs.push_back(1.1);
  xs.push_back(2.2);
  xs.push_back(3.2);
  xs.push_back(4.2);
  xs.push_back(5.2);
  pIntervals interval = Intervals::New(xs);
  EXPECT_EQ(5, interval->size());

  for(Intervals::const_iterator it = interval->begin();
      it != interval->end(); ++it) {
    cout << it->first << " ";
    cout << it->second << endl;
  }
  cout << endl;
  for(Intervals::const_iterator it = interval->begin() + 2;
      it != interval->begin() + 3 + 1; ++it) {
    cout << it->first << " ";
    cout << it->second << endl;
  }
}

TEST(TEST_DiscreteRep, construct) {
  DiscreteRep* d = NULL;
  EXPECT_NO_THROW(d = new DiscreteRep(10, 0, 2, 3));
  EXPECT_NO_THROW(delete d);
}

TEST(TEST_DiscreteRep, first) {
  int num_interval(10);
  int num_point(3);
  DiscreteRep d_rep1(num_interval, 0, 2, num_point);
  DiscreteRep d_rep2(num_interval, 1, 3, num_point);
  DiscreteRep d_rep3(num_interval, 3, 5, num_point);

  for(int j = 0; j < num_point; j++)
    {
      for(int i = 0; i < 2+1; i++)
	d_rep1.set_value(i, j, i + j + 0.0);
      for(int i = 1; i < 3+1; i++)
	d_rep2.set_value(i, j, i * j + 0.0);
    }

  // [0, 1, 2], [1, 2, 3], [2, 3, 4]
  // 5 + 14 + 29 = 48
  double res; bool has_value;
  DiscreteRepProduct(d_rep1, d_rep1, &res, &has_value);
  EXPECT_TRUE(has_value);
  EXPECT_DOUBLE_EQ(48.0, res);

  // [0, 1, 2], [1, 2, 3], [2, 3, 4]
  // (0+1+8) + (1+8+27) + (8+27+64) = 
  //  9 + 36 + 99 = 144
  DiscreteRepProduct(d_rep1, d_rep1, d_rep1, &res, &has_value);
  EXPECT_TRUE(has_value);
  EXPECT_DOUBLE_EQ(144.0, res);  

  // [0, 1, 2], [1, 2, 3], [2, 3, 4]
  // [0, 0, 0], [0, 1, 2], [0, 2, 4], ..
  //                2+ 6  + 0 + 6 + 16 = 30
  DiscreteRepProduct(d_rep1, d_rep2, &res, &has_value);
  EXPECT_TRUE(has_value);
  EXPECT_DOUBLE_EQ(30.0, res);

  DiscreteRepProduct(d_rep1, d_rep3, &res, &has_value);
  EXPECT_FALSE(has_value);

}

TEST(TEST_DiscreteRep, practice_bspline) {
  
  DiscreteRep d_rep(5, 1, 3, 3);
  SquaredFunc f;
  double xs_in_m1p1[] = {-0.9, 0.0, 0.9};
  
  //  d_rep.SetFunctionInM1P1(f, xs_in_m1p1, );
}

TEST(TEST_DiscreteRep, product) {
  int num_interval(10);
  int num_point(3);
  DiscreteRep d_rep1(num_interval, 0, 2, num_point);
  DiscreteRep d_rep2(num_interval, 1, 3, num_point);
  DiscreteRep d_rep3(num_interval, 3, 5, num_point);

  for(int j = 0; j < num_point; j++)
    {
      for(int i = 0; i < 2+1; i++)
	d_rep1.set_value(i, j, i + j + 0.0);
      for(int i = 1; i < 3+1; i++)
	d_rep2.set_value(i, j, i * j + 0.0);
    }

  // [0, 1, 2], [1, 2, 3], [2, 3, 4]
  // 5 + 14 + 29 = 48
  double res; bool has_value;
  DiscreteRepProduct(d_rep1, d_rep1, &res, &has_value);

  EXPECT_EQ(has_value, 
	    DiscreteRepExistOverlap(d_rep1, d_rep1));
  EXPECT_DOUBLE_EQ(res,
		   DiscreteRepProductValue(d_rep1, d_rep1));

  DiscreteRepProduct(d_rep1, d_rep2, &res, &has_value);

  EXPECT_EQ(has_value, 
	    DiscreteRepExistOverlap(d_rep1, d_rep2));
  EXPECT_DOUBLE_EQ(res,
		   DiscreteRepProductValue(d_rep1, d_rep2));
}



  /*
TEST(PreTest_DiscreteRep, function)
{

  // input argument
  SquaredFunc f;
  pIntervals intervals = Intervals::NewUniform(0.0, 1.0, 5);
  vector<double> xs_in_m1p1;
  xs_in_m1p1[0] = -0.25; 
  xs_in_m1p1[0] = 0.0; 
  xs_in_m1p1[0] = 2.5;

  // private
  double** data_;
  
  data_ = new double*[intervals.size()];
  for(int i = 0; i < intervals.size(); i++) {
    data_[i]
  }

  pDiscreteRep f_rep = 
    DiscreteRep::NewFromFunction(f, *intervals, xs_in_m1p1);

}
  */
