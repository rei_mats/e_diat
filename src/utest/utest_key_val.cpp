#include <src/utils/key_val.hpp>
#include <gtest/gtest.h>
using namespace std;

TEST(KeyValReader, IsInteger) 
{
  EXPECT_TRUE(IsInteger("123"));
  EXPECT_FALSE(IsInteger("123a"));
}

TEST(KeyValReader, IsNumber)
{
  EXPECT_TRUE(IsNumber("1.23"));
  EXPECT_TRUE(IsNumber("123"));
  EXPECT_TRUE(IsNumber("-12.3"));
  EXPECT_TRUE(IsNumber("+12.3"));
  EXPECT_FALSE(IsNumber("+12.3s"));
}

class TEST_KeyVal : public :: testing::Test
{
public:
  KeyVal* key_val;

  virtual void SetUp()
  {
    key_val = new KeyVal();
    key_val->Add("k1", 122);
    key_val->Add("k2", 1.20);
    key_val->Add("k3", "abc");
    key_val->Add("k4", "45");
    key_val->Add("k5", "1.5");

    key_val->AddCasting("k6", "2");
    key_val->AddCasting("k7", "2.2");
  }
};

TEST(Any, string)
{
  boost::any a = string("abc");
  string b = any_cast<string>(a);
  EXPECT_EQ("abc", b);
}

TEST_F(TEST_KeyVal, exist_key)
{
  EXPECT_TRUE(key_val->ExistKey("k1"));
  EXPECT_TRUE(key_val->ExistKey("k2"));
  EXPECT_TRUE(key_val->ExistKey("k3"));
  EXPECT_FALSE(key_val->ExistKey("k44"));

  EXPECT_ANY_THROW(key_val->CheckExistKey("k44"));
}

TEST_F(TEST_KeyVal, get)
{
  EXPECT_NO_THROW(key_val->Get<int>("k1"));
  EXPECT_EQ(122,
	    key_val->Get<int>("k1"));

  EXPECT_NO_THROW(key_val->Get<double>("k2"));
  EXPECT_DOUBLE_EQ(1.20,
		   key_val->Get<double>("k2"));

  EXPECT_NO_THROW(key_val->Get<string>("k3"));

  EXPECT_EQ("abc",
	    key_val->Get<string>("k3"));
  EXPECT_NO_THROW(key_val->Get<string>("k4"));
  EXPECT_EQ("45",
	    key_val->Get<string>("k4"));

  EXPECT_EQ("1.5",
	    key_val->Get<string>("k5"));

  EXPECT_ANY_THROW(key_val->Get<int>("k5"));

  EXPECT_EQ(2,   key_val->Get<int>("k6"));
  EXPECT_EQ("2", key_val->Get<string>("k6"));
  EXPECT_DOUBLE_EQ(2.2, key_val->Get<double>("k7"));
}

/* TEST_F(TEST_KeyVal, GetAsString)
{
  EXPECT_NO_THROW(key_val->GetAsString("k1"));
  EXPECT_EQ("122",
	    key_val->GetAsString("k1"));
}
*/

TEST_F(TEST_KeyVal, write)
{
  ofstream ofs("key_val_dat2.dat");
  key_val->Write(ofs);
  system("cat key_val_dat2.dat");
}

TEST_F(TEST_KeyVal, read)
{
 
  std::ifstream ifs("key_val_data.dat");
  KeyVal* key_val2 = new KeyVal();
  key_val2->Read(ifs);
  ifs.close();

  EXPECT_TRUE(key_val2->ExistKey("key1"));
  EXPECT_FALSE(key_val2->ExistKey("key44"));

}













