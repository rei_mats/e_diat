
// #include <src/utils/support_string.hpp>
#include <src/utils/key_val.hpp>
#include <gtest/gtest.h>

TEST(KeyValReader, is_integer) {
  EXPECT_TRUE(is_integer("123"));
  EXPECT_FALSE(is_integer("123a"));
}
TEST(KeyValReader, is_number) {
  EXPECT_TRUE(is_number("1.23"));
  EXPECT_TRUE(is_number("123"));
  EXPECT_TRUE(is_number("-12.3"));
  EXPECT_TRUE(is_number("+12.3"));
  EXPECT_FALSE(is_number("+12.3s"));
}
TEST(KeyValReader, reader) {

  KeyValReader reader(':');
  ifstream ifs("key_val_data.dat");
  reader.ReadFromStream(ifs);
  ifs.close();
  EXPECT_EQ(reader.Get("key1"), "123");
  EXPECT_EQ(reader.Get("key2"), "44");
  EXPECT_EQ(reader.Get("kkk34"), "bbbb");
  EXPECT_TRUE(reader.Exist("key1"));
  EXPECT_FALSE(reader.Exist("dkey1"));

  EXPECT_TRUE(reader.IsNumberData("key1"));
  EXPECT_TRUE(reader.IsIntegerData("key1"));
}
TEST(KeyValReader, check) {

  ifstream ifs("key_val_data.dat");
  KeyValReader reader(':', ifs);
  EXPECT_THROW(reader.CheckExist("key3"), string);

  try
    {
      reader.CheckExist("key3");
    }
  catch(string& e)
    {
      cout << e << endl;
    }
  
}
TEST(KeyValReader, OneOf) {

  ifstream ifs("key_val_data.dat");
  KeyValReader reader(':', ifs);
  vector<string> v;
  
  reader.Display();
  
  v.push_back("123");
  v.push_back("12");
  EXPECT_TRUE(reader.OneOf("key1", v));

  vector<string> vv;
  vv.push_back("a");
  vv.push_back("b");
  EXPECT_FALSE(reader.OneOf("key1", vv));

  try
    {
      reader.CheckOneOf("key1", vv);
    }
  catch(string& e)
    {
      cout << e << endl;
    }
    

}
TEST(KeyValReader, GetAs) {

  KeyValReader reader(':');
  ifstream ifs("key_val_data.dat");
  reader.ReadFromStream(ifs);

  int n = reader.CheckGet<int>("key1");
  EXPECT_EQ(123, n);

  try
    {
      reader.CheckGet<int>("kkk34");
    }
  catch(string& e)
    {
      cout << e << endl;
    }

}



