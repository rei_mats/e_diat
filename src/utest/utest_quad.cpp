#include <src/utils/quad.hpp>
#include <gtest/gtest.h>
#include <src/utils/timer.hpp>

TEST(a,b )
{
  EXPECT_DOUBLE_EQ(1.0 / sqrt(3.0),
		   GaussQuadZeroPoint(2,0));

  EXPECT_DOUBLE_EQ( sqrt(3.0 / 5.0),
		   GaussQuadZeroPoint(3,0));

  EXPECT_DOUBLE_EQ( 0.0,
		   GaussQuadZeroPoint(3,1));

  EXPECT_DOUBLE_EQ( -sqrt(3.0/5.0),
		    GaussQuadZeroPoint(3,2));
}
