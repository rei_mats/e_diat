#include <src/utils/quad.hpp>
#include <gtest/gtest.h>
#include <src/utils/timer.hpp>

class SquaredFunc
{
public:
  SquaredFunc(double _a, double _b):
    a(_a), b(_b) {}
  double operator()(double x)
  {
    return a * x*x + b;
  }
private:
  float a;
  float b;
  
};

TEST(SquaredFunc, func)
{
  SquaredFunc squared(2.0, 1.0);
  EXPECT_DOUBLE_EQ(9.0,
		   squared(2.0));
}

TEST(GaussQuad, points)
{
  EXPECT_ANY_THROW(GaussQuad::GetInstance().
		   zero_points(10));

  EXPECT_DOUBLE_EQ(0.0,
		   GaussQuad::GetInstance().
		   zero_points(1)[0]);

  EXPECT_DOUBLE_EQ(1.0 / sqrt(3.0),
		   GaussQuad::GetInstance().
		   zero_points(2)[0]);

  EXPECT_DOUBLE_EQ( sqrt(3.0 / 5.0),
		    GaussQuad::GetInstance().
		    zero_points(3)[0]);  
  EXPECT_DOUBLE_EQ( 0.0,
		    GaussQuad::GetInstance().
		    zero_points(3)[1]);  
  EXPECT_DOUBLE_EQ( -sqrt(3.0/5.0),
		    GaussQuad::GetInstance().
		    zero_points(3)[2]);  
}

TEST(IntegralInterval, test)
{
  pInterval I1 = Interval::New(1.0, 1.5);
  pInterval I2 = Interval::New(1.0, 1.5);
  pInterval I3 = Interval::New(1.5, 0.2);

  EXPECT_DOUBLE_EQ(1.0,
		   I1->left_edge());
  EXPECT_DOUBLE_EQ(1.5,
		   I1->right_edge());

  EXPECT_DOUBLE_EQ(0.2,
		   I3->left_edge());
  EXPECT_DOUBLE_EQ(1.5,
		   I3->right_edge());

  EXPECT_DOUBLE_EQ(0.5, I1->size());
  EXPECT_TRUE(I1 != I2);
  EXPECT_TRUE(I1 == I1);
}

TEST(GaussQuadrature, zero_point)
{

  EXPECT_DOUBLE_EQ(1.0 / sqrt(3.0),
		   GaussQuadZeroPoint(2,0,-1.0,1.0));

  EXPECT_DOUBLE_EQ( sqrt(3.0 / 5.0),
		   GaussQuadZeroPoint(3,0,-1.0,1.0));

  EXPECT_DOUBLE_EQ( 0.0,
		   GaussQuadZeroPoint(3,1,-1.0,1.0));

  EXPECT_DOUBLE_EQ( -sqrt(3.0/5.0),
		    GaussQuadZeroPoint(3,2,-1.0,1.0));
}

TEST(GaussQuadrature, construct)
{
  vector<double> fs;
  fs.push_back(1.0);
  fs.push_back(2.0);
  pInterval I1 = Interval::New(-1.0, +1.0);

  GaussQuadValues gq(fs, I1);
  EXPECT_DOUBLE_EQ(3.0,
		   GaussQuadInt(gq));
}

TEST(GaussQuadrature, minus_1_to_plus_1)
{
  // one variable function f(x)=x^2
  SquaredFunc f1(2.0, 1.0);
  SquaredFunc f2(1.0, 0.0);
  pInterval I1 = Interval::New(-1.0, +1.0);
  pInterval I2 = Interval::New(-1.0, +1.0);

  // function values on 3th gauss quadrature point
  //  { f(x_i)  | x_i <- Gauss Quadrature Point}
  pGaussQuadValues gq1 = GaussQuadValues::New(f1,3,I1);
  pGaussQuadValues gq2 = GaussQuadValues::New(f2,3,I1);
  pGaussQuadValues gq1p= GaussQuadValues::New(f1,3,I2);

  // calculate integrate with Gauss Quadrature
  // Int_-1^+1 (2x^2+1)dx = [2/3*x^3 + x]= 4/3+2=10/3;
  EXPECT_DOUBLE_EQ(10.0 / 3.0, 
		   GaussQuadInt(*gq1));

  // calculate integrate of product
  // S (2xx+1)xx dx = S(2x^4+x^2)dx = [2/5 x^5+1/3x^3]
  //                = 2 *(2/5+1/3) = 2 * 11/15 = 22/15
  EXPECT_DOUBLE_EQ(22.0/15.0,
		   GaussQuadInt(*gq1, *gq2));
		   
  // evaluation for different interval
  EXPECT_DOUBLE_EQ(0.0,
		   GaussQuadInt(*gq1, *gq1p));
}

TEST(GaussQuadrature, a_b)
{
  SquaredFunc f1(2.0, 1.0);
  SquaredFunc f2(1.0, 0.0);

  pInterval I1 = Interval::New(1.0, 2.0);
  
  pGaussQuadValues gq1 = GaussQuadValues::New(f1,3,I1);
  pGaussQuadValues gq2 = GaussQuadValues::New(f2,3,I1);

  // S (2x^4+x^2) 
  // = [2/5 x^5 +1/3 x^3]^2_1
  // = 64/5 + 8/3 - 2/5 - 1/3
  EXPECT_DOUBLE_EQ(64.0/5.0 + 8.0/3.0 
		   - 2.0/5.0 - 1.0/3.0,
		   GaussQuadInt(*gq1, *gq2));

}

TEST(GaussQuadrature, product)
{
  SquaredFunc f1(2.0, 1.0);
  SquaredFunc f2(1.0, 0.0);
  pInterval I1 = Interval::New(1.0, +2.0);

  pGaussQuadValues gq1 = GaussQuadValues::New(f1,3,I1);
  pGaussQuadValues gq2 = GaussQuadValues::New(f2,3,I1);
  
  GaussQuadValues gq3 = 
    ProductGaussQuadValue(*gq1, *gq2);
  
  EXPECT_DOUBLE_EQ(GaussQuadInt(*gq1, *gq2),
		   GaussQuadInt(gq3));

  pGaussQuadValues gq4 = 
    ProductGaussQuadValue2(*gq1, *gq2);
  EXPECT_DOUBLE_EQ(GaussQuadInt(*gq1, *gq2),
		   GaussQuadInt(*gq4));  

  EXPECT_DOUBLE_EQ
    (GaussQuadInt(*gq1, *gq1, *gq2),
     GaussQuadInt
     (ProductGaussQuadValue(*gq1,*gq1,*gq2)));
}

TEST(GaussQuadrature, product_time)
{
  SquaredFunc f1(2.0, 1.0);
  SquaredFunc f2(1.0, 0.0);
  pInterval I1 = Interval::New(1.0, +2.0);

  pGaussQuadValues gq1 = GaussQuadValues::New(f1,5,I1);
  pGaussQuadValues gq2 = GaussQuadValues::New(f2,5,I1);

  Timer timer;
  int num(100000);

  timer.Start("copy");
  for(int i = 0; i < num; i++)
    {
      GaussQuadValues gq3 = 
	ProductGaussQuadValue(*gq1, *gq2);
      GaussQuadInt(gq3);
    }
  timer.End("copy");

  timer.Start("pointer");
  for(int i = 0; i < num; i++)
    {
      pGaussQuadValues gq4 = 
	ProductGaussQuadValue2(*gq1, *gq2);  
      GaussQuadInt(*gq4);
    }
  timer.End("pointer");

  timer.Start("direct");
  for(int i = 0; i < num; i++)
    {
      GaussQuadInt(*gq1, *gq2);
    }  
  timer.End("direct");

  timer.Display();
}







