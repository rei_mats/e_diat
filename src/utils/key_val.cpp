#include "key_val.hpp"

bool IsInteger(const string& str)
{
  bool acc = true;
  for(string::const_iterator it = str.begin();
      it != str.end(); ++it)
    acc = acc & isdigit(*it);
  
  return acc;
}

bool IsNumber(const string& str)
{
  bool acc = true;
  for(string::const_iterator it = str.begin();
      it != str.end(); ++it)
    {
      char c = *it;
      acc = acc & (isdigit(c) ||c=='-'||c=='+'||c=='.'||c=='e');
    }
  return acc;
}

KeyVal::KeyVal() {}

template<>
double KeyVal::Get<double>(string key) const
{
  CheckExistKey(key);
  any x = key_val_map_.find(key)->second;
  return any_cast<double>(x);
}

template<>
int KeyVal::Get<int>(string key) const
{
  CheckExistKey(key);
  any x = key_val_map_.find(key)->second;
  return any_cast<int>(x);
}

template<>
string KeyVal::Get<string>(string key) const
{
  ConstIt it = key_val_map_.find(key);
  if (it == key_val_map_.end())
    {
      string msg;
      msg += "Error: KeyVal::GetAsString\n";
      msg += "invalid key =" + key;
      throw msg;
    }

  any val = it->second;  
  std::type_info const & t = val.type();
  string res;

  if(t == typeid(int))
    res = lexical_cast<string>(any_cast<int>(val));
  else if(t == typeid(double))
    res = lexical_cast<string>(any_cast<double>(val));
  else if(t == typeid(string))
    res = any_cast<string>(val);
  else
    {
      string msg;
      msg += "Error: KeyVal::GetAsString\n";
      msg += "invalid type.";
      msg += "key is " + key;
    }
  return res;
}

bool KeyVal::ExistKey (const string& key) const
{
  return key_val_map_.find(key) != key_val_map_.end();
}

bool KeyVal::CheckExistKey( const string& key) const
{
  bool exist = ExistKey(key);
  if(not exist)
    {
      string msg ;
      msg += "Error: Key does not exi\n";
      msg += "key is " + key;
      throw msg;
    }
  return exist;
}

void KeyVal::Add(const string& k, double t)
{ 
  key_val_map_[k]=double(t); 
}
void KeyVal::Add(const string& k, int t) 
{ 
  key_val_map_[k]=int(t); 
}
void KeyVal::Add(const string& k, const string& t) 
{ 
  key_val_map_[k]=string(t); 
}
void KeyVal::AddCasting(const string& k, 
			const string& v)
{

  if(IsInteger(v))
    {
      this->Add(k, lexical_cast<int>(v));
    }
  else if(IsNumber(v))
    {
      this->Add(k, lexical_cast<double>(v));
    }
  else
    {
      this->Add(k, v);
    }
}

void KeyVal::Read(ifstream& ifs, string sep)
{
  string line;
  while(getline(ifs, line))
    {
      if(line == "")
	continue;

      vector<string> str_vec;
      split(str_vec, line, is_any_of(sep));
      
      if(str_vec.size() != 2)
	{
	  string msg;
	  msg += "Error: KeyVal::Read\n";
	  msg += "error line is \n";
	  msg += line;
	  throw msg;
	}

      string key = trim_copy(str_vec[0]);
      string val = trim_copy(str_vec[1]);

      try
	{
	  this->AddCasting(key, val);
	}
      catch(...)
	{
	  string msg2;
	  msg2 += "Error: KeyVal::Read\n";
	  msg2 += "bad cast error\n";
	  msg2 += "error line is\n";
	  msg2 += line;
	  throw msg2;
	}
    }
}

void KeyVal::Write(ofstream& ofs) const
{
  for(ConstIt it = key_val_map_.begin();
      it != key_val_map_.end();
      ++it)
    {
      string k = it->first;
      string v = this->Get<string>(k);
      ofs << k << ": " << v << endl;
    }
}

