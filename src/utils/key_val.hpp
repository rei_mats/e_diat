#ifndef KEY_VAL_HPP
#define KEY_VAL_HPP

#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/any.hpp>

namespace
{
  using std::string;
  using std::ifstream;
  using std::ofstream;
  using std::map;
  using std::vector;
  using std::endl;
  using std::cout;

  using boost::any;
  using boost::any_cast;
  using boost::algorithm::split;
  using boost::algorithm::is_any_of;
  using boost::algorithm::trim_copy;
  using boost::lexical_cast;

  typedef map<string, any>::const_iterator ConstIt;
}

bool IsInteger(const string&);
bool IsNumber(const string&);

class KeyVal 
{
private:
  map<string, any> key_val_map_;

public:

  // ============ Constructor ===================
  KeyVal();

  // ============ Pure Function==================
  template<class T> T Get(string key) const;
  bool ExistKey (const string&) const;
  bool CheckExistKey(const string&) const;

  // ============ Side Effect ===================  
  void Add(const string& k, double t);
  void Add(const string& k, int t) ;
  void Add(const string& k, const string& t);
  void AddCasting(const string&, const string&);

  // ============ Input/Output ==================
  void Read(ifstream&, string sep=":");
  void Write(ofstream&) const;

};

#endif
