#include "quad.hpp"

double GaussQuadWeight(int i, int j)
{
  return GaussQuadValues::GetInstance().w(i, j);
}

double GaussQuadZeroPoint(int i, int j)
{
  return GaussQuadValues::GetInstance().x(i, j);
}

double GaussQuadZeroPoint(int i, int j, double a, double b)
{
  double x = GaussQuadZeroPoint(i, j);
  return (a - b) / 2.0 * x + (a + b) / 2.0;
}

GaussQuadValues::GaussQuadValues()
{
  double x21 = 1.0 / sqrt(3.0);
  double x31 = sqrt(3.0/5.0);
  double x41 = sqrt((3.0 - 2.0*sqrt(6.0/5.0))/7.0);
  double x42 = sqrt((3.0 + 2.0*sqrt(6.0/5.0))/7.0);
  double x51 = 1.0/3.0 * sqrt(5.0 + 2.0 * 
			      sqrt(10.0/7.0));
  double x52 = 1.0/3.0 * sqrt(5.0 - 2.0 * 
			      sqrt(10.0/7.0));
  double xs[] = {
    0.0, 
    x21, -x21,
    x31, 0.0,  -x31,
    x41, x42, -x42, -x41,
    x51, x52, 0.0, -x52, -x51};

  double w41 = (18.0 + sqrt(30.0)) / 36.0;
  double w42 = (18.0 - sqrt(30.0)) / 36.0;
  double w51 = (322.0 - 13.0 * sqrt(70.0)) / 900.0;
  double w52 = (322.0 + 13.0 * sqrt(70.0)) / 900.0;
  double w53 = 128.0 / 225.0;

  double ws[] = {
    2.0,
    1.0, 1.0,
    5.0/9.0, 8.0/9.0, 5.0/9.0,
    w41, w42, w42, w41,
    w51, w52, w53, w52, w51 };

  for(int i = 0; i < 15; i++) {
    xs_[i] = xs[i];
    ws_[i] = ws[i];
  }
}

int ijToIdx(int i, int j) { return (i - 1) * i / 2 + j; }

double GaussQuadValues::x(int i, int j) const
{
  return xs_[ ijToIdx(i,j) ];
}
double GaussQuadValues::w(int i, int j) const
{
  return ws_[ ijToIdx(i,j)];
}



