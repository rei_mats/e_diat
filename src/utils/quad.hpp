#ifndef QUAD_HPP
#define QUAD_HPP

#include <math.h>
#include <iostream>

namespace
{
  using std::cout;
  using std::endl;
}

double GaussQuadWeight(int, int);
double GaussQuadZeroPoint(int, int);
double GaussQuadZeroPoint(int, int, double, double);

/**
 * singleton class which compute point for Gauss Quadrature
 */
class GaussQuadValues
{
private:
  double xs_[20]; /* zero point of Rugendre function*/
  double ws_[20]; /* weight*/
public:
  GaussQuadValues();
  double x (int i, int j) const;
  double w (int i, int j) const;
  static GaussQuadValues& GetInstance()
  { 
    static GaussQuadValues gauss_quad;
    return gauss_quad;
  }
};

#endif
