#include "quad.hpp"

double GaussQuadZeroPoint(int order, int i, double x0, double x1)
{
  const vector<double>& xs = GaussQuad::GetInstance().zero_points(order);
  return (x1-x0) / 2.0 * xs[i] + (x1+x0)/2.0;
}

double GaussQuadWeight(int order, int i)
{
  const vector<double>& ws = GaussQuad::GetInstance().weights(order);
  return ws[i];
}

double GaussQuadInt(const GaussQuadValues& gq)
{
  double a = gq.interval()->left_edge();
  double b = gq.interval()->right_edge();

  const vector<double>& fs = gq.ref_fxn_values();
  const vector<double>& ws = GaussQuad::GetInstance().
    weights(gq.n_order());

  typedef vector<double>::const_iterator It;
  double acc = 0.0;
  It it_fn = fs.begin();
  It it_wn = ws.begin();
  while(it_fn != fs.end() && it_wn != ws.end())
    {
      acc += (*it_fn) * (*it_wn);
      ++it_fn;
      ++it_wn;
    }

  return (b - a) / 2.0 * acc;
}

double GaussQuadInt(const GaussQuadValues& gq1,
		    const GaussQuadValues& gq2)
{
  if(gq1.interval() != gq2.interval())
    return 0.0;

  double a = gq1.interval()->left_edge();
  double b = gq1.interval()->right_edge();

  const vector<double>& fs1 = gq1.ref_fxn_values();
  const vector<double>& fs2 = gq2.ref_fxn_values();
  int n1 = gq1.n_order();
  int n2 = gq2.n_order();

  if(n1 != n2)
    {
      throw std::logic_error("GaussQuadInt, order must be eq.");
	
    }

  const vector<double>& ws = GaussQuad::GetInstance().
    weights(n1);

  typedef vector<double>::const_iterator It;
  double acc = 0.0;
  It it_fn1 = fs1.begin();
  It it_fn2 = fs2.begin();
  It it_wn = ws.begin();
  while(it_fn1 != fs1.end() && 
	it_fn2 != fs2.end() && 
	it_wn != ws.end())
    {
      acc += (*it_fn2) * (*it_fn1) * (*it_wn);
      ++it_fn1;
      ++it_fn2;
      ++it_wn;
    }

  return (b-a) / 2.0 * acc;
}

double GaussQuadInt(const GaussQuadValues& gq1,
		    const GaussQuadValues& gq2,
		    const GaussQuadValues& gq3)
{

  if(gq1.interval() != gq2.interval() ||
     gq1.interval() != gq3.interval())
    return 0.0;

  double a = gq1.interval()->left_edge();
  double b = gq1.interval()->right_edge();

  const vector<double>& fs1 = gq1.ref_fxn_values();
  const vector<double>& fs2 = gq2.ref_fxn_values();
  const vector<double>& fs3 = gq3.ref_fxn_values();

  int n1 = gq1.n_order();
  int n2 = gq2.n_order();
  int n3 = gq3.n_order();

  if(n1 != n2 || n1 != n3)
    {
      throw std::logic_error("GaussQuadInt, order must be eq.");
    }

  const vector<double>& ws = GaussQuad::GetInstance().
    weights(n1);

  typedef vector<double>::const_iterator It;
  double acc = 0.0;
  It it_fn1 = fs1.begin();
  It it_fn2 = fs2.begin();
  It it_fn3 = fs3.begin();  
  It it_wn = ws.begin();
  while(it_fn1 != fs1.end() && 
	it_fn2 != fs2.end() && 
	it_fn3 != fs3.end() && 
	it_wn != ws.end())
    {
      acc += (*it_fn3) * (*it_fn2) * (*it_fn1) 
	* (*it_wn);
      ++it_fn1;
      ++it_fn2;
      ++it_fn3;
      ++it_wn;
    }

  return (b-a) / 2.0 * acc;

}

double ScaleForInterval(const Interval& interval,
			double x)
{
  double a = interval.left_edge();
  double b = interval.right_edge();
  return (b-a)/2.0 * x + (a+b)/2.0;
}

pGaussQuadValues ProductGaussQuadValue2
(const GaussQuadValues& a, 
 const GaussQuadValues& b) 
{
  // extract data
  int n1 = a.n_order();
  int n2 = b.n_order();
  const vector<double>& fs1 = a.ref_fxn_values();
  const vector<double>& fs2 = b.ref_fxn_values();

  // error check
  if(a.interval() != b.interval())
    {
      string msg = "ProductGaussQuadValue,";
      msg += "interval must be same.";
      throw std::logic_error(msg);
    }

  if(n1 != n2)
    {
      string msg = "ProductGaussQuadValue,";
      msg += "order must be same";
      throw std::logic_error(msg);
    }

  // construct
  typedef vector<double>::const_iterator It;
  vector<double> fs;
  It it_fn1 = fs1.begin();
  It it_fn2 = fs2.begin();
  while(it_fn1 != fs1.end() && 
	it_fn2 != fs2.end())
    {
      fs.push_back( (*it_fn2) * (*it_fn1) );
      ++it_fn1;
      ++it_fn2;
    }  
  
  GaussQuadValues* ab = new GaussQuadValues
    (fs, a.interval());
  
  return pGaussQuadValues(ab);
}

pGaussQuadValues ProductGaussQuadValue2
(const GaussQuadValues& a, 
 const GaussQuadValues& b,
 const GaussQuadValues& c)
{
  return ProductGaussQuadValue2
    (c, *ProductGaussQuadValue2(a, b));
}

GaussQuadValues ProductGaussQuadValue
(const GaussQuadValues& a, 
 const GaussQuadValues& b)
{
 // extract data
  int n1 = a.n_order();
  int n2 = b.n_order();
  const vector<double>& fs1 = a.ref_fxn_values();
  const vector<double>& fs2 = b.ref_fxn_values();

  // error check
  if(a.interval() != b.interval())
    {
      string msg = "ProductGaussQuadValue,";
      msg += "interval must be same.";
      throw std::logic_error(msg);
    }

  if(n1 != n2)
    {
      string msg = "ProductGaussQuadValue,";
      msg += "order must be same";
      throw std::logic_error(msg);
    }

  // construct
  typedef vector<double>::const_iterator It;
  vector<double> fs;
  It it_fn1 = fs1.begin();
  It it_fn2 = fs2.begin();
  while(it_fn1 != fs1.end() && 
	it_fn2 != fs2.end())
    {
      fs.push_back( (*it_fn2) * (*it_fn1) );
      ++it_fn1;
      ++it_fn2;
    }  
  
  GaussQuadValues ab(fs, a.interval());
  
  return ab;
}

GaussQuadValues ProductGaussQuadValue
(const GaussQuadValues& a, 
 const GaussQuadValues& b,
 const GaussQuadValues& c)
{
  return ProductGaussQuadValue
    (a, ProductGaussQuadValue(b, c));
}

 
GaussQuad::GaussQuad()
{
  max_order_ = 5;
  double x21 = 1.0 / sqrt(3.0);
  double x31 = sqrt(3.0/5.0);
  double x41 = sqrt((3.0 - 2.0*sqrt(6.0/5.0))/7.0);
  double x42 = sqrt((3.0 + 2.0*sqrt(6.0/5.0))/7.0);
  double x51 = 1.0/3.0 * sqrt(5.0 + 2.0 * 
			      sqrt(10.0/7.0));
  double x52 = 1.0/3.0 * sqrt(5.0 - 2.0 * 
			      sqrt(10.0/7.0));
  double xs[] = {
    0.0, 
    x21, -x21,
    x31, 0.0,  -x31,
    x41, x42, -x42, -x41,
    x51, x52, 0.0, -x52, -x51};

  double w41 = (18.0 + sqrt(30.0)) / 36.0;
  double w42 = (18.0 - sqrt(30.0)) / 36.0;
  double w51 = (322.0 - 13.0 * sqrt(70.0)) / 900.0;
  double w52 = (322.0 + 13.0 * sqrt(70.0)) / 900.0;
  double w53 = 128.0 / 225.0;

  double ws[] = {
    2.0,
    1.0, 1.0,
    5.0/9.0, 8.0/9.0, 5.0/9.0,
    w41, w42, w42, w41,
    w51, w52, w53, w52, w51 };

  for(int n = 0; n < max_order_; n++)
    {
      vector<double> tmp_xs;
      vector<double> tmp_ws;
      for(int k = 0; k < n+1; k++)
	{
	  int idx = n*(n+1)/2+k;
	  tmp_xs.push_back(xs[idx]);
	  tmp_ws.push_back(ws[idx]);
	}
      zero_points_list_.push_back(tmp_xs);
      weights_list_.push_back(tmp_ws);
    }
}

Interval::Interval(double a, double b)
{
  if(a > b)
    {
      left_edge_  = b;
      right_edge_ = a;
    }
  else
    {
      left_edge_ = a;
      right_edge_= b;
    }
}

GaussQuadValues::GaussQuadValues
(
 vector<double>& _fxn, 
 pInterval _interval)
{
  fxn_values_.swap(_fxn);
  n_order_ = fxn_values_.size();
  interval_ = _interval;
}

