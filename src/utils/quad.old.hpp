#ifndef QUAD_HPP
#define QUAD_HPP

#include <boost/shared_ptr.hpp>
#include <vector>
#include <stdexcept>
#include <math.h>

namespace
{
  using std::vector;
  using std::cout;
  using std::endl;
  using std::string;
  using boost::shared_ptr;
}

class Interval;
class GaussQuadValues;
typedef shared_ptr<Interval> pInterval;
typedef shared_ptr<GaussQuadValues> pGaussQuadValues;

// GaussQuadZeroPoint(deg,j,x0,x1);
double GaussQuadZeroPoint(int, int, double,double);
double GaussQuadWeight(int order, int i);

/** numerical integrate with Gauss Quadrature */
double GaussQuadInt(const GaussQuadValues&);
double GaussQuadInt(const GaussQuadValues& gq1,
		    const GaussQuadValues& gq2);
double GaussQuadInt(const GaussQuadValues& gq1,
		    const GaussQuadValues& gq2,
		    const GaussQuadValues& gq3);

/** scale variable x in (-1,1) to interval */
double ScaleForInterval(const Interval& interval, 
			double x);

/** production of GaussQuadValue */
GaussQuadValues ProductGaussQuadValue
(const GaussQuadValues& a, 
 const GaussQuadValues& b);

GaussQuadValues ProductGaussQuadValue
(const GaussQuadValues& a, 
 const GaussQuadValues& b,
 const GaussQuadValues& c);

pGaussQuadValues ProductGaussQuadValue2
(const GaussQuadValues& a, 
 const GaussQuadValues& b);

/**
 *  get gauss quadrature points and weights
 *  this is implemented by  singleton pattern.
 */
class GaussQuad
{
private:
  GaussQuad();
  vector<vector<double> > zero_points_list_;
  vector<vector<double> > weights_list_;
  int max_order_;

public:
  const vector<double>& zero_points(int n) const
  {
    if(n < 1 || n > max_order_ )
      {
	throw std::out_of_range
	  ("GaussQuad::zero_points");
      }
    return zero_points_list_[n-1];
  }
  const vector<double>& weights(int n) const
  {
    if(n < 1 || n > max_order_ )
      {
	throw std::out_of_range
	  ("GaussQuad::zero_points");
      }
    return weights_list_[n-1];
  }
  static GaussQuad& GetInstance() 
  { 
    static GaussQuad gauss_quad;
    return gauss_quad;
  }
};

/**
 *  integral interval
 *  to be removed
 */
class Interval
{
public:
  // ============= Constructor ================
  Interval(double a, double b);
  static pInterval New(double a, double b) 
  {
    return pInterval(new Interval(a, b));
  }

  // ============= Pure Functional ============
  inline double left_edge() const 
  { 
    return left_edge_; 
  }
  inline double right_edge() const 
  {
    return right_edge_;
  }
  inline double size() const 
  { 
    return right_edge_ - left_edge_;
  }

private:
  // ========== Field Member ==============
  double left_edge_;
  double right_edge_;

};

/**
 *  function array of gauss quadrature points
 */
class GaussQuadValues
{
  /* ========= Constructor ============ */
public:
  GaussQuadValues(){}
  GaussQuadValues(vector<double>& _fxn, 
		  pInterval _interval);

  template<class F>
  static pGaussQuadValues New(F f, int n, 
			      pInterval _interval)  
  {
    GaussQuadValues* p = new GaussQuadValues();
    p->interval_ = _interval;
    p->Init(f, n);
    return pGaussQuadValues(p);
  }

  /* ========= Pure Function =========== */
  inline int n_order() const { return n_order_; }
  inline const vector<double>& ref_fxn_values() const
  { 
    return fxn_values_;
  }
  inline pInterval interval() const { 
    return interval_; }

  /* ========= Side Effect ============= */
  template<class F>
  void Init(F f, int n)
  {
    n_order_ = n;
    const vector<double>& xs = 
      GaussQuad::GetInstance().zero_points(n);
    
    for(vector<double>::const_iterator it 
	  = xs.begin();
	it != xs.end();
	++it)
      {
	double xn = ScaleForInterval(*interval_,
				     *it);
	fxn_values_.push_back(f(xn));
      }
  }
  
private:
  /* ======== Member Field ============*/
  int n_order_;

  /* {f(xi) | xi<-quad points}*/
  vector<double> fxn_values_; 

  pInterval interval_;
};


#endif
