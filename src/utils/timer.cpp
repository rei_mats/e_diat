#include "timer.hpp"

Timer::Status Timer::GetStatus(string label) {
  Status status =
    (label_data_list.find(label) == 
     label_data_list.end()) ?
    kTimerStatusNone :
    label_data_list[label].status;
  return status;
}

void Timer::Start(string label) 
{
  if(GetStatus(label) != kTimerStatusNone) {
    string msg = "In Timer::Start, ";
    msg += label + " is already started or ended.";
    throw std::runtime_error(msg);
  }
  TimeData data;
  data.id = current_id;
  ++current_id;
  data.t0 = clock();
  data.status = kTimerStatusStart;
  
  label_data_list.insert(make_pair(label, data));
}

void Timer::End(string label) {

  if(GetStatus(label) == kTimerStatusNone) {
    string msg = "In Timer::End, ";
    msg += label + " is not started.";
    throw std::runtime_error(msg);
  }
  if(GetStatus(label) == kTimerStatusEnd) {
    string msg = "In Timer::End, ";
    msg += label  + " is already ended.";
    throw std::runtime_error(msg);
  }

  label_data_list[label].t1 = clock();
  label_data_list[label].status = kTimerStatusEnd;

}

void Timer::Display() {

  cout << "label : time / s" << endl;

  for(map<string, TimeData>::iterator it = 
	label_data_list.begin();
      it != label_data_list.end();
      ++it)
    {
      string   label = it->first;
      TimeData data  = it->second;

      if(GetStatus(label) != kTimerStatusEnd) {
	string msg = "In Timer::Display, ";
	msg += label + " is not ended.";
	throw std::runtime_error(msg);
      }

      double dt = (1.0*(data.t1 - data.t0)) / 
	CLOCKS_PER_SEC;
      cout << label << " : " << dt << endl;	
    }
}
