#ifndef TIMER_HPP
#define TIMER_HPP

#include <map>
#include <string>
#include <iostream>
#include <stdexcept>
#include <time.h>

namespace
{
  using std::map;
  using std::string;
  using std::cout;
  using std::endl;
}

class Timer {

  enum Status {
    kTimerStatusNone,
    kTimerStatusStart,
    kTimerStatusEnd,
  };  

  struct TimeData {
    int id;
    clock_t t0;
    clock_t t1;
    Status status;
  };

private:
  /* ====== Member Field ====== */
  int current_id;
  map<string, TimeData> label_data_list;

public:
  /* ====== Constructor ======= */
  Timer() : current_id(0) {}

public:
  /* ===== Pure Functional ======= */
  Status GetStatus(string label);

  /* ====== Side Effect ========== */
  void Reset() { label_data_list.clear(); }
  void Start(string label) ;
  void End(string label);

  /* ====== Input / Output ======= */
  void Display();

};
#endif



